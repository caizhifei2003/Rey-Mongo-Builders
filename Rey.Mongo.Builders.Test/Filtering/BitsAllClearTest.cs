﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class BitsAllClearTest {
        [Fact(DisplayName = "Filter.BitsAllClear")]
        public void TestBitsAllClear() {
            var json = Filters<Person>.BitsAllClear(x => x.Height, 0xff)
                .ToJsonString();

            var expected = Expector.GetFilter("BitsAllClear");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllClear.Not")]
        public void TestBitsAllClearNot() {
            var json = Filters<Person>.Not(not => not
                .BitsAllClear(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAllClear.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllClear.And")]
        public void TestBitsAllClearAnd() {
            var json = Filters<Person>.And(and => and
                .BitsAllClear(x => x.Height, 0xff)
                .BitsAllClear(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAllClear.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllClear.Or")]
        public void TestBitsAllClearOr() {
            var json = Filters<Person>.Or(or => or
                .BitsAllClear(x => x.Height, 0xff)
                .BitsAllClear(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAllClear.Or");
            Assert.Equal(expected, json);
        }
    }
}
