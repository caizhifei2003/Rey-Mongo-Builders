﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class WhereTest {
        [Fact(DisplayName = "Filter.Where")]
        public void TestWhere() {
            var json = Filters<Person>.Where(x => x.Height > 0xff)
                .ToJsonString();

            var expected = Expector.GetFilter("Where");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Where.Not")]
        public void TestWhereNot() {
            var json = Filters<Person>.Not(not => not
                .Where(x => x.Height > 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Where.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Where.And")]
        public void TestWhereAnd() {
            var json = Filters<Person>.And(and => and
                .Where(x => x.Height > 100)
                .Where(x => x.Height < 200)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Where.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Where.Or")]
        public void TestWhereOr() {
            var json = Filters<Person>.Or(or => or
                .Where(x => x.Height > 100)
                .Where(x => x.Height < 200)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Where.Or");
            Assert.Equal(expected, json);
        }
    }
}
