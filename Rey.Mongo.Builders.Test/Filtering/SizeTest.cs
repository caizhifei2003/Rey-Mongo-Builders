﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class SizeTest {
        [Fact(DisplayName = "Filter.Size")]
        public void TestSize() {
            var json = Filters<Person>.Size(x => x.Height, 0xff)
                .ToJsonString();

            var expected = Expector.GetFilter("Size");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Size.Not")]
        public void TestSizeNot() {
            var json = Filters<Person>.Not(not => not
                .Size(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Size.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Size.And")]
        public void TestSizeAnd() {
            var json = Filters<Person>.And(and => and
                .Size(x => x.Height, 0xff)
                .Size(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Size.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Size.Or")]
        public void TestSizeOr() {
            var json = Filters<Person>.Or(or => or
                .Size(x => x.Height, 0xff)
                .Size(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Size.Or");
            Assert.Equal(expected, json);
        }
    }
}
