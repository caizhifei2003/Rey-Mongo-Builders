﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class GtTest {
        [Fact(DisplayName = "Filter.Gt")]
        public void TestGt() {
            var json = Filters<Person>.Gt(x => x.Age, 18)
                .ToJsonString();

            var expected = Expector.GetFilter("Gt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gt.Not")]
        public void TestGtNot() {
            var json = Filters<Person>.Not(not => not
                .Gt(x => x.Age, 18)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Gt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gt.And")]
        public void TestGtAnd() {
            var json = Filters<Person>.And(and => and
                .Gt(x => x.Age, 18)
                .Gt(x => x.Height, 175)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Gt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gt.Or")]
        public void TestGtOr() {
            var json = Filters<Person>.Or(or => or
                .Gt(x => x.Age, 18)
                .Gt(x => x.Height, 175)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Gt.Or");
            Assert.Equal(expected, json);
        }
    }
}
