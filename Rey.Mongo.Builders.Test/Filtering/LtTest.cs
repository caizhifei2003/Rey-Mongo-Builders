﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class LtTest {
        [Fact(DisplayName = "Filter.Lt")]
        public void TestLt() {
            var json = Filters<Person>.Lt(x => x.Age, 18)
                .ToJsonString();

            var expected = Expector.GetFilter("Lt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lt.Not")]
        public void TestLtNot() {
            var json = Filters<Person>.Not(not => not
                .Lt(x => x.Age, 18)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Lt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lt.And")]
        public void TestLtAnd() {
            var json = Filters<Person>.And(and => and
                .Lt(x => x.Age, 18)
                .Lt(x => x.Height, 175)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Lt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lt.Or")]
        public void TestLtOr() {
            var json = Filters<Person>.Or(or => or
                .Lt(x => x.Age, 18)
                .Lt(x => x.Height, 175)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Lt.Or");
            Assert.Equal(expected, json);
        }
    }
}
