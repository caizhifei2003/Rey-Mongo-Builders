﻿using MongoDB.Bson;
using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class AnyGteTest {
        [Fact(DisplayName = "Filter.AnyGte")]
        public void TestAnyGte() {
            var json = Filters<Person>.AnyGte(x => x.BWH, 91.38)
                .ToJsonString();

            var expected = Expector.GetFilter("AnyGte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGte.Not")]
        public void TestAnyGteNot() {
            var json = Filters<Person>.Not(not => not
                .AnyGte(x => x.BWH, 91.38)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyGte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGte.And")]
        public void TestAnyGteAnd() {
            var json = Filters<Person>.And(and => and
                .AnyGte(x => x.BWH, 91.38)
                .AnyGte(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyGte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGte.Or")]
        public void TestAnyGteOr() {
            var json = Filters<Person>.Or(or => or
                .AnyGte(x => x.BWH, 91.38)
                .AnyGte(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyGte.Or");
            Assert.Equal(expected, json);
        }
    }
}
