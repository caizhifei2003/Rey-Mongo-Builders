﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class BitsAnySetTest {
        [Fact(DisplayName = "Filter.BitsAnySet")]
        public void TestBitsAnySet() {
            var json = Filters<Person>.BitsAnySet(x => x.Height, 0xff)
                .ToJsonString();

            var expected = Expector.GetFilter("BitsAnySet");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnySet.Not")]
        public void TestBitsAnySetNot() {
            var json = Filters<Person>.Not(not => not
                .BitsAnySet(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAnySet.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnySet.And")]
        public void TestBitsAnySetAnd() {
            var json = Filters<Person>.And(and => and
                .BitsAnySet(x => x.Height, 0xff)
                .BitsAnySet(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAnySet.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnySet.Or")]
        public void TestBitsAnySetOr() {
            var json = Filters<Person>.Or(or => or
                .BitsAnySet(x => x.Height, 0xff)
                .BitsAnySet(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAnySet.Or");
            Assert.Equal(expected, json);
        }
    }
}
