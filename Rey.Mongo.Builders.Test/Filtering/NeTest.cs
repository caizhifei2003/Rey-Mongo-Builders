﻿using MongoDB.Bson;
using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class NeTest {
        [Fact(DisplayName = "Filter.Ne")]
        public void TestNe() {
            var json = Filters<Person>.Ne(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .ToJsonString();

            var expected = Expector.GetFilter("Ne");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Ne.Not")]
        public void TestNeNot() {
            var json = Filters<Person>.Not(not => not
                .Ne(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Ne.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Ne.And")]
        public void TestNeAnd() {
            var json = Filters<Person>.And(and => and
                .Ne(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .Ne(x => x.Name, "kevin")
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Ne.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Ne.Or")]
        public void TestNeOr() {
            var json = Filters<Person>.Or(or => or
                .Ne(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .Ne(x => x.Name, "kevin")
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Ne.Or");
            Assert.Equal(expected, json);
        }
    }
}
