﻿using MongoDB.Bson;
using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class AnyGtTest {
        [Fact(DisplayName = "Filter.AnyGt")]
        public void TestAnyGt() {
            var json = Filters<Person>.AnyGt(x => x.BWH, 91.38)
                .ToJsonString();

            var expected = Expector.GetFilter("AnyGt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGt.Not")]
        public void TestAnyGtNot() {
            var json = Filters<Person>.Not(not => not
                .AnyGt(x => x.BWH, 91.38)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyGt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGt.And")]
        public void TestAnyGtAnd() {
            var json = Filters<Person>.And(and => and
                .AnyGt(x => x.BWH, 91.38)
                .AnyGt(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyGt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGt.Or")]
        public void TestAnyGtOr() {
            var json = Filters<Person>.Or(or => or
                .AnyGt(x => x.BWH, 91.38)
                .AnyGt(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyGt.Or");
            Assert.Equal(expected, json);
        }
    }
}
