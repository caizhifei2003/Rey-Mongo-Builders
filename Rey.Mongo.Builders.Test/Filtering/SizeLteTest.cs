﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class SizeLteTest {
        [Fact(DisplayName = "Filter.SizeLte")]
        public void TestSizeLte() {
            var json = Filters<Person>.SizeLte(x => x.Height, 0xff)
                .ToJsonString();

            var expected = Expector.GetFilter("SizeLte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLte.Not")]
        public void TestSizeLteNot() {
            var json = Filters<Person>.Not(not => not
                .SizeLte(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeLte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLte.And")]
        public void TestSizeLteAnd() {
            var json = Filters<Person>.And(and => and
                .SizeLte(x => x.Height, 0xff)
                .SizeLte(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeLte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLte.Or")]
        public void TestSizeLteOr() {
            var json = Filters<Person>.Or(or => or
                .SizeLte(x => x.Height, 0xff)
                .SizeLte(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeLte.Or");
            Assert.Equal(expected, json);
        }
    }
}
