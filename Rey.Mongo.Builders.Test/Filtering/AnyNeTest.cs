﻿using MongoDB.Bson;
using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class AnyNeTest {
        [Fact(DisplayName = "Filter.AnyNe")]
        public void TestAnyNe() {
            var json = Filters<Person>.AnyNe(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .ToJsonString();

            var expected = Expector.GetFilter("AnyNe");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyNe.Not")]
        public void TestAnyNeNot() {
            var json = Filters<Person>.Not(not => not
                .AnyNe(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyNe.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyNe.And")]
        public void TestAnyNeAnd() {
            var json = Filters<Person>.And(and => and
                .AnyNe(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .AnyNe(x => x.Parents, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyNe.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyNe.Or")]
        public void TestAnyNeOr() {
            var json = Filters<Person>.Or(or => or
                .AnyNe(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .AnyNe(x => x.Parents, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyNe.Or");
            Assert.Equal(expected, json);
        }
    }
}
