﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class BitsAnyClearTest {
        [Fact(DisplayName = "Filter.BitsAnyClear")]
        public void TestBitsAnyClear() {
            var json = Filters<Person>.BitsAnyClear(x => x.Height, 0xff)
                .ToJsonString();

            var expected = Expector.GetFilter("BitsAnyClear");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnyClear.Not")]
        public void TestBitsAnyClearNot() {
            var json = Filters<Person>.Not(not => not
                .BitsAnyClear(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAnyClear.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnyClear.And")]
        public void TestBitsAnyClearAnd() {
            var json = Filters<Person>.And(and => and
                .BitsAnyClear(x => x.Height, 0xff)
                .BitsAnyClear(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAnyClear.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnyClear.Or")]
        public void TestBitsAnyClearOr() {
            var json = Filters<Person>.Or(or => or
                .BitsAnyClear(x => x.Height, 0xff)
                .BitsAnyClear(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAnyClear.Or");
            Assert.Equal(expected, json);
        }
    }
}
