﻿using MongoDB.Bson;
using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class AnyEqTest {
        [Fact(DisplayName = "Filter.AnyEq")]
        public void TestAnyEq() {
            var json = Filters<Person>.AnyEq(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .ToJsonString();

            var expected = Expector.GetFilter("AnyEq");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyEq.Not")]
        public void TestAnyEqNot() {
            var json = Filters<Person>.Not(not => not
                .AnyEq(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyEq.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyEq.And")]
        public void TestAnyEqAnd() {
            var json = Filters<Person>.And(and => and
                .AnyEq(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .AnyEq(x => x.Parents, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyEq.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyEq.Or")]
        public void TestAnyEqOr() {
            var json = Filters<Person>.Or(or => or
                .AnyEq(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .AnyEq(x => x.Parents, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyEq.Or");
            Assert.Equal(expected, json);
        }
    }
}
