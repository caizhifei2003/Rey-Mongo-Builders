﻿using MongoDB.Bson;
using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class AnyLtTest {
        [Fact(DisplayName = "Filter.AnyLt")]
        public void TestAnyLt() {
            var json = Filters<Person>.AnyLt(x => x.BWH, 91.38)
                .ToJsonString();

            var expected = Expector.GetFilter("AnyLt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLt.Not")]
        public void TestAnyLtNot() {
            var json = Filters<Person>.Not(not => not
                .AnyLt(x => x.BWH, 91.38)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyLt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLt.And")]
        public void TestAnyLtAnd() {
            var json = Filters<Person>.And(and => and
                .AnyLt(x => x.BWH, 91.38)
                .AnyLt(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyLt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLt.Or")]
        public void TestAnyLtOr() {
            var json = Filters<Person>.Or(or => or
                .AnyLt(x => x.BWH, 91.38)
                .AnyLt(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyLt.Or");
            Assert.Equal(expected, json);
        }
    }
}
