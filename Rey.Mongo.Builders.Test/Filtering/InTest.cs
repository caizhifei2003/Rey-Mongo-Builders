﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class InTest {
        [Fact(DisplayName = "Filter.In")]
        public void TestIn() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var json = Filters<Person>.In(x => x.Name, names)
               .ToJsonString();

            var expected = Expector.GetFilter("In");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.In.Not")]
        public void TestInNot() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var json = Filters<Person>.Not(not => not
                .In(x => x.Name, names)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("In.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.In.And")]
        public void TestInAnd() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var ages = new int[] { 15, 18, 28 };
            var json = Filters<Person>.And(and => and
                .In(x => x.Name, names)
                .In(x => x.Age, ages)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("In.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.In.Or")]
        public void TestInOr() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var ages = new int[] { 15, 18, 28 };
            var json = Filters<Person>.Or(or => or
                .In(x => x.Name, names)
                .In(x => x.Age, ages)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("In.Or");
            Assert.Equal(expected, json);
        }
    }
}
