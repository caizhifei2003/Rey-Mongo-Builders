﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class BitsAllSetTest {
        [Fact(DisplayName = "Filter.BitsAllSet")]
        public void TestBitsAllSet() {
            var json = Filters<Person>.BitsAllSet(x => x.Height, 0xff)
                .ToJsonString();

            var expected = Expector.GetFilter("BitsAllSet");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllSet.Not")]
        public void TestBitsAllSetNot() {
            var json = Filters<Person>.Not(not => not
                .BitsAllSet(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAllSet.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllSet.And")]
        public void TestBitsAllSetAnd() {
            var json = Filters<Person>.And(and => and
                .BitsAllSet(x => x.Height, 0xff)
                .BitsAllSet(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAllSet.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllSet.Or")]
        public void TestBitsAllSetOr() {
            var json = Filters<Person>.Or(or => or
                .BitsAllSet(x => x.Height, 0xff)
                .BitsAllSet(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("BitsAllSet.Or");
            Assert.Equal(expected, json);
        }
    }
}
