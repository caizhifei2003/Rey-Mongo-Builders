﻿using MongoDB.Bson;
using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class AnyLteTest {
        [Fact(DisplayName = "Filter.AnyLte")]
        public void TestAnyLte() {
            var json = Filters<Person>.AnyLte(x => x.BWH, 91.38)
                .ToJsonString();

            var expected = Expector.GetFilter("AnyLte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLte.Not")]
        public void TestAnyLteNot() {
            var json = Filters<Person>.Not(not => not
                .AnyLte(x => x.BWH, 91.38)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyLte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLte.And")]
        public void TestAnyLteAnd() {
            var json = Filters<Person>.And(and => and
                .AnyLte(x => x.BWH, 91.38)
                .AnyLte(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyLte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLte.Or")]
        public void TestAnyLteOr() {
            var json = Filters<Person>.Or(or => or
                .AnyLte(x => x.BWH, 91.38)
                .AnyLte(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyLte.Or");
            Assert.Equal(expected, json);
        }
    }
}
