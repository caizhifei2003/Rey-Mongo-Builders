﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class LteTest {
        [Fact(DisplayName = "Filter.Lte")]
        public void TestLte() {
            var json = Filters<Person>.Lte(x => x.Age, 18)
                .ToJsonString();

            var expected = Expector.GetFilter("Lte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lte.Not")]
        public void TestLteNot() {
            var json = Filters<Person>.Not(not => not
                .Lte(x => x.Age, 18)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Lte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lte.And")]
        public void TestLteAnd() {
            var json = Filters<Person>.And(and => and
                .Lte(x => x.Age, 18)
                .Lte(x => x.Height, 175)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Lte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lte.Or")]
        public void TestLteOr() {
            var json = Filters<Person>.Or(or => or
                .Lte(x => x.Age, 18)
                .Lte(x => x.Height, 175)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Lte.Or");
            Assert.Equal(expected, json);
        }
    }
}
