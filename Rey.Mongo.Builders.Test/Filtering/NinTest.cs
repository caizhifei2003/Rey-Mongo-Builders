﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class NinTest {
        [Fact(DisplayName = "Filter.Nin")]
        public void TestNin() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var json = Filters<Person>.Nin(x => x.Name, names)
               .ToJsonString();

            var expected = Expector.GetFilter("Nin");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Nin.Not")]
        public void TestNinNot() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var json = Filters<Person>.Not(not => not
                .Nin(x => x.Name, names)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Nin.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Nin.And")]
        public void TestNinAnd() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var ages = new int[] { 15, 18, 28 };
            var json = Filters<Person>.And(and => and
                .Nin(x => x.Name, names)
                .Nin(x => x.Age, ages)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Nin.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Nin.Or")]
        public void TestNinOr() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var ages = new int[] { 15, 18, 28 };
            var json = Filters<Person>.Or(or => or
                .Nin(x => x.Name, names)
                .Nin(x => x.Age, ages)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Nin.Or");
            Assert.Equal(expected, json);
        }
    }
}
