﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class ModTest {
        [Fact(DisplayName = "Filter.Mod")]
        public void TestMod() {
            var json = Filters<Person>.Mod(x => x.Height, 10, 3)
                .ToJsonString();

            var expected = Expector.GetFilter("Mod");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Mod.Not")]
        public void TestModNot() {
            var json = Filters<Person>.Not(not => not
                .Mod(x => x.Height, 10, 3)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Mod.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Mod.And")]
        public void TestModAnd() {
            var json = Filters<Person>.And(and => and
                .Mod(x => x.Height, 10, 3)
                .Mod(x => x.Height, 10, 3)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Mod.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Mod.Or")]
        public void TestModOr() {
            var json = Filters<Person>.Or(or => or
                .Mod(x => x.Height, 10, 3)
                .Mod(x => x.Height, 10, 3)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Mod.Or");
            Assert.Equal(expected, json);
        }
    }
}
