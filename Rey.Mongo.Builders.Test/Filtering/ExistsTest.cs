﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class ExistsTest {
        [Fact(DisplayName = "Filter.Exists")]
        public void TestExists() {
            var json = Filters<Person>.Exists(x => x.Name)
                .ToJsonString();

            var expected = Expector.GetFilter("Exists");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Exists.Not")]
        public void TestExistsNot() {
            var json = Filters<Person>.Not(not => not
                .Exists(x => x.Name)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Exists.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Exists.And")]
        public void TestExistsAnd() {
            var json = Filters<Person>.And(and => and
                .Exists(x => x.Name)
                .Exists(x => x.Name)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Exists.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Exists.Or")]
        public void TestExistsOr() {
            var json = Filters<Person>.Or(or => or
                .Exists(x => x.Name)
                .Exists(x => x.Name)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Exists.Or");
            Assert.Equal(expected, json);
        }
    }
}
