﻿using MongoDB.Bson;
using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class EqTest {
        [Fact(DisplayName = "Filter.Eq")]
        public void TestEq() {
            var json = Filters<Person>.Eq(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .ToJsonString();

            var expected = Expector.GetFilter("Eq");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Eq.Not")]
        public void TestEqNot() {
            var json = Filters<Person>.Not(not => not
                .Eq(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Eq.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Eq.And")]
        public void TestEqAnd() {
            var json = Filters<Person>.And(and => and
                .Eq(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .Eq(x => x.Name, "kevin")
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Eq.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Eq.Or")]
        public void TestEqOr() {
            var json = Filters<Person>.Or(or => or
                .Eq(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .Eq(x => x.Name, "kevin")
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Eq.Or");
            Assert.Equal(expected, json);
        }
    }
}
