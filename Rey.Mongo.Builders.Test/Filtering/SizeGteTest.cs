﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class SizeGteTest {
        [Fact(DisplayName = "Filter.SizeGte")]
        public void TestSizeGte() {
            var json = Filters<Person>.SizeGte(x => x.Height, 0xff)
                .ToJsonString();

            var expected = Expector.GetFilter("SizeGte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGte.Not")]
        public void TestSizeGteNot() {
            var json = Filters<Person>.Not(not => not
                .SizeGte(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeGte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGte.And")]
        public void TestSizeGteAnd() {
            var json = Filters<Person>.And(and => and
                .SizeGte(x => x.Height, 0xff)
                .SizeGte(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeGte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGte.Or")]
        public void TestSizeGteOr() {
            var json = Filters<Person>.Or(or => or
                .SizeGte(x => x.Height, 0xff)
                .SizeGte(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeGte.Or");
            Assert.Equal(expected, json);
        }
    }
}
