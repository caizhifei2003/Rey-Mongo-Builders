﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class SizeGtTest {
        [Fact(DisplayName = "Filter.SizeGt")]
        public void TestSizeGt() {
            var json = Filters<Person>.SizeGt(x => x.Height, 0xff)
                .ToJsonString();

            var expected = Expector.GetFilter("SizeGt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGt.Not")]
        public void TestSizeGtNot() {
            var json = Filters<Person>.Not(not => not
                .SizeGt(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeGt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGt.And")]
        public void TestSizeGtAnd() {
            var json = Filters<Person>.And(and => and
                .SizeGt(x => x.Height, 0xff)
                .SizeGt(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeGt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGt.Or")]
        public void TestSizeGtOr() {
            var json = Filters<Person>.Or(or => or
                .SizeGt(x => x.Height, 0xff)
                .SizeGt(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeGt.Or");
            Assert.Equal(expected, json);
        }
    }
}
