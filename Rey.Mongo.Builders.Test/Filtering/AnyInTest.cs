﻿using MongoDB.Bson;
using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using System.Linq;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class AnyInTest {
        [Fact(DisplayName = "Filter.AnyIn")]
        public void TestAnyIn() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filters<Person>.AnyIn(x => x.Children, value)
                .ToJsonString();

            var expected = Expector.GetFilter("AnyIn");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyIn.Not")]
        public void TestAnyInNot() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filters<Person>.Not(not => not
                .AnyIn(x => x.Children, value)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyIn.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyIn.And")]
        public void TestAnyInAnd() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filters<Person>.And(and => and
                .AnyIn(x => x.Children, value)
                .AnyIn(x => x.Parents, value)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyIn.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyIn.Or")]
        public void TestAnyInOr() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filters<Person>.Or(or => or
                .AnyIn(x => x.Children, value)
                .AnyIn(x => x.Parents, value)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("AnyIn.Or");
            Assert.Equal(expected, json);
        }
    }
}
