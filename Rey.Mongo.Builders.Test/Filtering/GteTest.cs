﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class GteTest {
        [Fact(DisplayName = "Filter.Gte")]
        public void TestGte() {
            var json = Filters<Person>.Gte(x => x.Age, 18)
                .ToJsonString();

            var expected = Expector.GetFilter("Gte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gte.Not")]
        public void TestGteNot() {
            var json = Filters<Person>.Not(not => not
                .Gte(x => x.Age, 18)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Gte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gte.And")]
        public void TestGteAnd() {
            var json = Filters<Person>.And(and => and
                .Gte(x => x.Age, 18)
                .Gte(x => x.Height, 175)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Gte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gte.Or")]
        public void TestGteOr() {
            var json = Filters<Person>.Or(or => or
                .Gte(x => x.Age, 18)
                .Gte(x => x.Height, 175)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("Gte.Or");
            Assert.Equal(expected, json);
        }
    }
}
