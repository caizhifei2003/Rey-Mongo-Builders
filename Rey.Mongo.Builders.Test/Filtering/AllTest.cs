﻿using MongoDB.Bson;
using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using System.Linq;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class AllTest {
        [Fact(DisplayName = "Filter.All")]
        public void TestAll() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filters<Person>.All(x => x.Children, value)
                .ToJsonString();

            var expected = Expector.GetFilter("All");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.All.Not")]
        public void TestAllNot() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filters<Person>.Not(not => not
                .All(x => x.Children, value)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("All.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.All.And")]
        public void TestAllAnd() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filters<Person>.And(and => and
                .All(x => x.Children, value)
                .All(x => x.Parents, value)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("All.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.All.Or")]
        public void TestAllOr() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filters<Person>.Or(or => or
                .All(x => x.Children, value)
                .All(x => x.Parents, value)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("All.Or");
            Assert.Equal(expected, json);
        }
    }
}
