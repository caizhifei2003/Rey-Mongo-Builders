﻿using Rey.Mongo.Builders.Fitering;
using Rey.Mongo.Builders.Test.Expectation;
using Rey.Mongo.Builders.Test.Models;
using Xunit;

namespace Rey.Mongo.Builders.Test.Filtering {
    public class SizeLtTest {
        [Fact(DisplayName = "Filter.SizeLt")]
        public void TestSizeLt() {
            var json = Filters<Person>.SizeLt(x => x.Height, 0xff)
                .ToJsonString();

            var expected = Expector.GetFilter("SizeLt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLt.Not")]
        public void TestSizeLtNot() {
            var json = Filters<Person>.Not(not => not
                .SizeLt(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeLt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLt.And")]
        public void TestSizeLtAnd() {
            var json = Filters<Person>.And(and => and
                .SizeLt(x => x.Height, 0xff)
                .SizeLt(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeLt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLt.Or")]
        public void TestSizeLtOr() {
            var json = Filters<Person>.Or(or => or
                .SizeLt(x => x.Height, 0xff)
                .SizeLt(x => x.Height, 0xff)
            )
            .ToJsonString();

            var expected = Expector.GetFilter("SizeLt.Or");
            Assert.Equal(expected, json);
        }
    }
}
