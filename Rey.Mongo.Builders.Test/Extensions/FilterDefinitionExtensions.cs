﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Rey.Mongo.Builders.Test {
    public static class FilterDefinitionExtensions {
        public static BsonDocument ToBsonDocument<T>(this FilterDefinition<T> filter) {
            var serializerRegistry = BsonSerializer.SerializerRegistry;
            var documentSerializer = serializerRegistry.GetSerializer<T>();
            return filter.Render(documentSerializer, serializerRegistry);
        }

        public static string ToJsonString<T>(this FilterDefinition<T> filter) {
            return ToBsonDocument(filter).ToString();
        }
    }
}
