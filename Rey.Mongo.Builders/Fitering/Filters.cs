﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders.Fitering {
    public static class Filters<TModel> {
        #region Logical

        public static FilterDefinition<TModel> Not(Func<INotFilterBuilder<TModel>, IFilterBuilder<TModel>> build) {
            return FilterCreator<TModel>.Not(build).Build();
        }

        public static FilterDefinition<TModel> And(Action<IAndFilterBuilder<TModel>> build) {
            return FilterCreator<TModel>.And(build).Build();
        }

        public static FilterDefinition<TModel> Or(Action<IOrFilterBuilder<TModel>> build) {
            return FilterCreator<TModel>.Or(build).Build();
        }

        #endregion Logical

        public static FilterDefinition<TModel> Eq<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Eq(field, value).Build();
        }

        public static FilterDefinition<TModel> Ne<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Ne(field, value).Build();
        }

        public static FilterDefinition<TModel> Gt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Gt(field, value).Build();
        }

        public static FilterDefinition<TModel> Gte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Gte(field, value).Build();
        }

        public static FilterDefinition<TModel> Lt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Lt(field, value).Build();
        }

        public static FilterDefinition<TModel> Lte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Lte(field, value).Build();
        }


        public static FilterDefinition<TModel> In<TItem>(Expression<Func<TModel, TItem>> field, IEnumerable<TItem> values) {
            return FilterCreator<TModel>.In(field, values).Build();
        }

        public static FilterDefinition<TModel> Nin<TItem>(Expression<Func<TModel, TItem>> field, IEnumerable<TItem> values) {
            return FilterCreator<TModel>.Nin(field, values).Build();
        }


        public static FilterDefinition<TModel> AnyEq<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyEq(field, value).Build();
        }

        public static FilterDefinition<TModel> AnyNe<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyNe(field, value).Build();
        }

        public static FilterDefinition<TModel> AnyGt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyGt(field, value).Build();
        }

        public static FilterDefinition<TModel> AnyGte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyGte(field, value).Build();
        }

        public static FilterDefinition<TModel> AnyLt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyLt(field, value).Build();
        }

        public static FilterDefinition<TModel> AnyLte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyLte(field, value).Build();
        }


        public static FilterDefinition<TModel> All<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return FilterCreator<TModel>.All(field, values).Build();
        }

        public static FilterDefinition<TModel> AnyIn<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return FilterCreator<TModel>.AnyIn(field, values).Build();
        }

        public static FilterDefinition<TModel> AnyNin<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return FilterCreator<TModel>.AnyNin(field, values).Build();
        }

        public static FilterDefinition<TModel> ElemMatch<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, Func<IUnaryFilterBuilder<TItem>, IFilterBuilder<TItem>> build) {
            return FilterCreator<TModel>.ElemMatch(field, build).Build();
        }

        public static FilterDefinition<TModel> Exists(Expression<Func<TModel, object>> field, bool exists = true) {
            return FilterCreator<TModel>.Exists(field, exists).Build();
        }

        public static FilterDefinition<TModel> Mod(Expression<Func<TModel, object>> field, long modulus, long remainder) {
            return FilterCreator<TModel>.Mod(field, modulus, remainder).Build();
        }

        public static FilterDefinition<TModel> BitsAllClear(Expression<Func<TModel, object>> field, long bitmask) {
            return FilterCreator<TModel>.BitsAllClear(field, bitmask).Build();
        }

        public static FilterDefinition<TModel> BitsAllSet(Expression<Func<TModel, object>> field, long bitmask) {
            return FilterCreator<TModel>.BitsAllSet(field, bitmask).Build();
        }

        public static FilterDefinition<TModel> BitsAnyClear(Expression<Func<TModel, object>> field, long bitmask) {
            return FilterCreator<TModel>.BitsAnyClear(field, bitmask).Build();
        }

        public static FilterDefinition<TModel> BitsAnySet(Expression<Func<TModel, object>> field, long bitmask) {
            return FilterCreator<TModel>.BitsAnySet(field, bitmask).Build();
        }

        public static FilterDefinition<TModel> Size(Expression<Func<TModel, object>> field, int size) {
            return FilterCreator<TModel>.Size(field, size).Build();
        }

        public static FilterDefinition<TModel> SizeGt(Expression<Func<TModel, object>> field, int size) {
            return FilterCreator<TModel>.SizeGt(field, size).Build();
        }

        public static FilterDefinition<TModel> SizeGte(Expression<Func<TModel, object>> field, int size) {
            return FilterCreator<TModel>.SizeGte(field, size).Build();
        }

        public static FilterDefinition<TModel> SizeLt(Expression<Func<TModel, object>> field, int size) {
            return FilterCreator<TModel>.SizeLt(field, size).Build();
        }

        public static FilterDefinition<TModel> SizeLte(Expression<Func<TModel, object>> field, int size) {
            return FilterCreator<TModel>.SizeLte(field, size).Build();
        }

        public static FilterDefinition<TModel> Where(Expression<Func<TModel, bool>> expression) {
            return FilterCreator<TModel>.Where(expression).Build();
        }
    }
}
