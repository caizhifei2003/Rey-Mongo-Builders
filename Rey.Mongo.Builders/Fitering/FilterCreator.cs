﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders.Fitering {
    public static class FilterCreator<TModel> {
        #region Logical

        public static IFilterBuilder<TModel> Not(Func<INotFilterBuilder<TModel>, IFilterBuilder<TModel>> build) {
            return new NotFilterBuilder<TModel>(build);
        }

        public static IFilterBuilder<TModel> And(Action<IAndFilterBuilder<TModel>> build) {
            return new AndFilterBuilder<TModel>(build);
        }

        public static IFilterBuilder<TModel> Or(Action<IOrFilterBuilder<TModel>> build) {
            return new OrFilterBuilder<TModel>(build);
        }

        #endregion Logical

        public static IFilterBuilder<TModel> Eq<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return new EqFilterBuilder<TModel, TField>(field, value);
        }

        public static IFilterBuilder<TModel> Ne<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return new NeFilterBuilder<TModel, TField>(field, value);
        }

        public static IFilterBuilder<TModel> Gt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return new GtFilterBuilder<TModel, TField>(field, value);
        }

        public static IFilterBuilder<TModel> Gte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return new GteFilterBuilder<TModel, TField>(field, value);
        }

        public static IFilterBuilder<TModel> Lt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return new LtFilterBuilder<TModel, TField>(field, value);
        }

        public static IFilterBuilder<TModel> Lte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return new LteFilterBuilder<TModel, TField>(field, value);
        }


        public static IFilterBuilder<TModel> In<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values) {
            return new InFilterBuilder<TModel, TField>(field, values);
        }

        public static IFilterBuilder<TModel> Nin<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values) {
            return new NinFilterBuilder<TModel, TField>(field, values);
        }


        public static IFilterBuilder<TModel> AnyEq<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return new AnyEqFilterBuilder<TModel, TItem>(field, value);
        }

        public static IFilterBuilder<TModel> AnyNe<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return new AnyNeFilterBuilder<TModel, TItem>(field, value);
        }

        public static IFilterBuilder<TModel> AnyGt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return new AnyGtFilterBuilder<TModel, TItem>(field, value);
        }

        public static IFilterBuilder<TModel> AnyGte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return new AnyGteFilterBuilder<TModel, TItem>(field, value);
        }

        public static IFilterBuilder<TModel> AnyLt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return new AnyLtFilterBuilder<TModel, TItem>(field, value);
        }

        public static IFilterBuilder<TModel> AnyLte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return new AnyLteFilterBuilder<TModel, TItem>(field, value);
        }


        public static IFilterBuilder<TModel> All<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return new AllFilterBuilder<TModel, TItem>(field, values);
        }

        public static IFilterBuilder<TModel> AnyIn<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return new AnyInFilterBuilder<TModel, TItem>(field, values);
        }

        public static IFilterBuilder<TModel> AnyNin<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return new AnyNinFilterBuilder<TModel, TItem>(field, values);
        }


        public static IFilterBuilder<TModel> ElemMatch<TItem>(
            Expression<Func<TModel, IEnumerable<TItem>>> field,
            Func<IUnaryFilterBuilder<TItem>, IFilterBuilder<TItem>> build
            ) {
            return new ElemMatchFilterBuilder<TModel, TItem>(field, build);
        }

        public static IFilterBuilder<TModel> Exists(Expression<Func<TModel, object>> field, bool exists = true) {
            return new ExistsFilterBuilder<TModel>(field, exists);
        }

        public static IFilterBuilder<TModel> Mod(Expression<Func<TModel, object>> field, long modulus, long remainder) {
            return new ModFilterBuilder<TModel>(field, (modulus, remainder));
        }

        public static IFilterBuilder<TModel> BitsAllClear(Expression<Func<TModel, object>> field, long bitmask) {
            return new BitsAllClearFilterBuilder<TModel>(field, bitmask);
        }

        public static IFilterBuilder<TModel> BitsAllSet(Expression<Func<TModel, object>> field, long bitmask) {
            return new BitsAllSetFilterBuilder<TModel>(field, bitmask);
        }

        public static IFilterBuilder<TModel> BitsAnyClear(Expression<Func<TModel, object>> field, long bitmask) {
            return new BitsAnyClearFilterBuilder<TModel>(field, bitmask);
        }

        public static IFilterBuilder<TModel> BitsAnySet(Expression<Func<TModel, object>> field, long bitmask) {
            return new BitsAnySetFilterBuilder<TModel>(field, bitmask);
        }

        public static IFilterBuilder<TModel> Size(Expression<Func<TModel, object>> field, int size) {
            return new SizeFilterBuilder<TModel>(field, size);
        }

        public static IFilterBuilder<TModel> SizeGt(Expression<Func<TModel, object>> field, int size) {
            return new SizeGtFilterBuilder<TModel>(field, size);
        }

        public static IFilterBuilder<TModel> SizeGte(Expression<Func<TModel, object>> field, int size) {
            return new SizeGteFilterBuilder<TModel>(field, size);
        }

        public static IFilterBuilder<TModel> SizeLt(Expression<Func<TModel, object>> field, int size) {
            return new SizeLtFilterBuilder<TModel>(field, size);
        }

        public static IFilterBuilder<TModel> SizeLte(Expression<Func<TModel, object>> field, int size) {
            return new SizeLteFilterBuilder<TModel>(field, size);
        }

        public static IFilterBuilder<TModel> Where(Expression<Func<TModel, bool>> expression) {
            return new WhereFilterBuilder<TModel>(expression);
        }
    }
}
