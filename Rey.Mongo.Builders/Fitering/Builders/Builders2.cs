﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders.Fitering {
    public abstract class MongoFilterBuilder2<TModel, TField> : MongoFilterBuilder<TModel, Expression<Func<TModel, TField>>, IEnumerable<TField>> {
        public MongoFilterBuilder2(Expression<Func<TModel, TField>> field, IEnumerable<TField> value)
            : base(field, value) {
        }
    }

    public class InFilterBuilder<TModel, TField> : MongoFilterBuilder2<TModel, TField> {
        public InFilterBuilder(Expression<Func<TModel, TField>> field, IEnumerable<TField> value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.In(this.Field, this.Value);
        }
    }

    public class NinFilterBuilder<TModel, TField> : MongoFilterBuilder2<TModel, TField> {
        public NinFilterBuilder(Expression<Func<TModel, TField>> field, IEnumerable<TField> value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.Nin(this.Field, this.Value);
        }
    }
}
