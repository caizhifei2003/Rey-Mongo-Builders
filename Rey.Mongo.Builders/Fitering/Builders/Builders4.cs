﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders.Fitering {
    public abstract class MongoFilterBuilder4<TModel, TField> : MongoFilterBuilder<TModel, Expression<Func<TModel, IEnumerable<TField>>>, IEnumerable<TField>> {
        public MongoFilterBuilder4(Expression<Func<TModel, IEnumerable<TField>>> field, IEnumerable<TField> value)
            : base(field, value) {
        }
    }

    public class AllFilterBuilder<TModel, TField> : MongoFilterBuilder4<TModel, TField> {
        public AllFilterBuilder(Expression<Func<TModel, IEnumerable<TField>>> field, IEnumerable<TField> value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.All(this.Field, this.Value);
        }
    }

    public class AnyInFilterBuilder<TModel, TField> : MongoFilterBuilder4<TModel, TField> {
        public AnyInFilterBuilder(Expression<Func<TModel, IEnumerable<TField>>> field, IEnumerable<TField> value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.AnyIn(this.Field, this.Value);
        }
    }

    public class AnyNinFilterBuilder<TModel, TField> : MongoFilterBuilder4<TModel, TField> {
        public AnyNinFilterBuilder(Expression<Func<TModel, IEnumerable<TField>>> field, IEnumerable<TField> value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.AnyNin(this.Field, this.Value);
        }
    }
}
