﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders.Fitering {
    public class ElemMatchFilterBuilder<TModel, TItem> : MongoFilterBuilder<TModel, Expression<Func<TModel, IEnumerable<TItem>>>, Func<IUnaryFilterBuilder<TItem>, IFilterBuilder<TItem>>> {
        public ElemMatchFilterBuilder(Expression<Func<TModel, IEnumerable<TItem>>> field, Func<IUnaryFilterBuilder<TItem>, IFilterBuilder<TItem>> value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            var builder = new DefaultUnaryFilterBuilder<TItem>(this.Value);
            return Builders<TModel>.Filter.ElemMatch(this.Field, builder.Build());
        }
    }
}
