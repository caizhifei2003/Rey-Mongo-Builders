﻿using MongoDB.Driver;
using System;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders.Fitering {
    public abstract class MongoFilterBuilder1<TModel, TField> : MongoFilterBuilder<TModel, Expression<Func<TModel, TField>>, TField> {
        public MongoFilterBuilder1(Expression<Func<TModel, TField>> field, TField value)
            : base(field, value) {
        }
    }

    public class EqFilterBuilder<TModel, TField> : MongoFilterBuilder1<TModel, TField> {
        public EqFilterBuilder(Expression<Func<TModel, TField>> field, TField value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.Eq(this.Field, this.Value);
        }
    }

    public class NeFilterBuilder<TModel, TField> : MongoFilterBuilder1<TModel, TField> {
        public NeFilterBuilder(Expression<Func<TModel, TField>> field, TField value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.Ne(this.Field, this.Value);
        }
    }

    public class GtFilterBuilder<TModel, TField> : MongoFilterBuilder1<TModel, TField> {
        public GtFilterBuilder(Expression<Func<TModel, TField>> field, TField value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.Gt(this.Field, this.Value);
        }
    }

    public class GteFilterBuilder<TModel, TField> : MongoFilterBuilder1<TModel, TField> {
        public GteFilterBuilder(Expression<Func<TModel, TField>> field, TField value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.Gte(this.Field, this.Value);
        }
    }

    public class LtFilterBuilder<TModel, TField> : MongoFilterBuilder1<TModel, TField> {
        public LtFilterBuilder(Expression<Func<TModel, TField>> field, TField value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.Lt(this.Field, this.Value);
        }
    }

    public class LteFilterBuilder<TModel, TField> : MongoFilterBuilder1<TModel, TField> {
        public LteFilterBuilder(Expression<Func<TModel, TField>> field, TField value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.Lte(this.Field, this.Value);
        }
    }
}
