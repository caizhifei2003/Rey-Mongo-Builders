﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders.Fitering {
    public abstract class MongoFilterBuilder3<TModel, TItem> : MongoFilterBuilder<TModel, Expression<Func<TModel, IEnumerable<TItem>>>, TItem> {
        public MongoFilterBuilder3(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value)
            : base(field, value) {
        }
    }

    public class AnyEqFilterBuilder<TModel, TItem> : MongoFilterBuilder3<TModel, TItem> {
        public AnyEqFilterBuilder(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.AnyEq(this.Field, this.Value);
        }
    }

    public class AnyNeFilterBuilder<TModel, TItem> : MongoFilterBuilder3<TModel, TItem> {
        public AnyNeFilterBuilder(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.AnyNe(this.Field, this.Value);
        }
    }

    public class AnyGtFilterBuilder<TModel, TItem> : MongoFilterBuilder3<TModel, TItem> {
        public AnyGtFilterBuilder(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.AnyGt(this.Field, this.Value);
        }
    }

    public class AnyGteFilterBuilder<TModel, TItem> : MongoFilterBuilder3<TModel, TItem> {
        public AnyGteFilterBuilder(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.AnyGte(this.Field, this.Value);
        }
    }

    public class AnyLtFilterBuilder<TModel, TItem> : MongoFilterBuilder3<TModel, TItem> {
        public AnyLtFilterBuilder(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.AnyLt(this.Field, this.Value);
        }
    }

    public class AnyLteFilterBuilder<TModel, TItem> : MongoFilterBuilder3<TModel, TItem> {
        public AnyLteFilterBuilder(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.AnyLte(this.Field, this.Value);
        }
    }
}
