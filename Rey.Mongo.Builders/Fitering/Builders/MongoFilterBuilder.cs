﻿namespace Rey.Mongo.Builders.Fitering {
    public abstract class MongoFilterBuilder<TModel, TField, TValue> : FilterBuilder<TModel> {
        protected TField Field { get; }
        protected TValue Value { get; }

        public MongoFilterBuilder(TField field, TValue value) {
            this.Field = field;
            this.Value = value;
        }
    }

    public abstract class MongoFilterBuilder<TModel, TField> : FilterBuilder<TModel> {
        protected TField Field { get; }

        public MongoFilterBuilder(TField field) {
            this.Field = field;
        }
    }
}
