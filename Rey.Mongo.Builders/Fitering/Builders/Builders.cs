﻿using MongoDB.Driver;
using System;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders.Fitering {
    public class ExistsFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, object>>, bool> {
        public ExistsFilterBuilder(Expression<Func<TModel, object>> field, bool value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.Exists(this.Field, this.Value);
        }
    }

    public class ModFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, object>>, (long modulus, long remainder)> {
        public ModFilterBuilder(Expression<Func<TModel, object>> field, (long modulus, long remainder) value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.Mod(this.Field, this.Value.modulus, this.Value.remainder);
        }
    }

    public class BitsAllClearFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, object>>, long> {
        public BitsAllClearFilterBuilder(Expression<Func<TModel, object>> field, long value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.BitsAllClear(this.Field, this.Value);
        }
    }

    public class BitsAllSetFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, object>>, long> {
        public BitsAllSetFilterBuilder(Expression<Func<TModel, object>> field, long value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.BitsAllSet(this.Field, this.Value);
        }
    }

    public class BitsAnyClearFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, object>>, long> {
        public BitsAnyClearFilterBuilder(Expression<Func<TModel, object>> field, long value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.BitsAnyClear(this.Field, this.Value);
        }
    }

    public class BitsAnySetFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, object>>, long> {
        public BitsAnySetFilterBuilder(Expression<Func<TModel, object>> field, long value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.BitsAnySet(this.Field, this.Value);
        }
    }

    public class SizeFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, object>>, int> {
        public SizeFilterBuilder(Expression<Func<TModel, object>> field, int value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.Size(this.Field, this.Value);
        }
    }

    public class SizeGtFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, object>>, int> {
        public SizeGtFilterBuilder(Expression<Func<TModel, object>> field, int value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.SizeGt(this.Field, this.Value);
        }
    }

    public class SizeGteFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, object>>, int> {
        public SizeGteFilterBuilder(Expression<Func<TModel, object>> field, int value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.SizeGte(this.Field, this.Value);
        }
    }

    public class SizeLtFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, object>>, int> {
        public SizeLtFilterBuilder(Expression<Func<TModel, object>> field, int value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.SizeLt(this.Field, this.Value);
        }
    }

    public class SizeLteFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, object>>, int> {
        public SizeLteFilterBuilder(Expression<Func<TModel, object>> field, int value)
            : base(field, value) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.SizeLte(this.Field, this.Value);
        }
    }

    public class WhereFilterBuilder<TModel> : MongoFilterBuilder<TModel, Expression<Func<TModel, bool>>> {
        public WhereFilterBuilder(Expression<Func<TModel, bool>> field)
            : base(field) {
        }

        public override FilterDefinition<TModel> Build() {
            return Builders<TModel>.Filter.Where(this.Field);
        }
    }
}
