﻿using MongoDB.Driver;
using System;

namespace Rey.Mongo.Builders.Fitering {
    public abstract class FilterBuilder<TModel> : IFilterBuilder<TModel> {
        public abstract FilterDefinition<TModel> Build();

        #region Logical

        public IFilterBuilder<TModel> Not(Func<INotFilterBuilder<TModel>, IFilterBuilder<TModel>> build) {
            return new NotFilterBuilder<TModel>(build);
        }

        public IFilterBuilder<TModel> And(Action<IAndFilterBuilder<TModel>> build) {
            return new AndFilterBuilder<TModel>(build);
        }

        public IFilterBuilder<TModel> Or(Action<IOrFilterBuilder<TModel>> build) {
            return new OrFilterBuilder<TModel>(build);
        }

        #endregion Logical
    }
}
