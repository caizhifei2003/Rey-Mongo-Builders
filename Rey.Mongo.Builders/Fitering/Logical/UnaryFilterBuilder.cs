﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders.Fitering {
    public abstract class UnaryFilterBuilder<TModel, TBuilder> : FilterBuilder<TModel>, IUnaryFilterBuilder<TModel>
        where TBuilder : class, IUnaryFilterBuilder<TModel> {
        private Func<TBuilder, IFilterBuilder<TModel>> _build;

        public UnaryFilterBuilder(Func<TBuilder, IFilterBuilder<TModel>> build) {
            this._build = build;
        }

        public override FilterDefinition<TModel> Build() {
            var builder = this._build.Invoke(this as TBuilder);
            if (builder == null)
                throw new InvalidOperationException("returned builder is null");

            var filter = builder.Build();
            return this.Build(filter);
        }

        protected abstract FilterDefinition<TModel> Build(FilterDefinition<TModel> filter);

        public IFilterBuilder<TModel> Eq<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Eq(field, value);
        }

        public IFilterBuilder<TModel> Ne<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Ne(field, value);
        }

        public IFilterBuilder<TModel> Gt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Gt(field, value);
        }

        public IFilterBuilder<TModel> Gte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Gte(field, value);
        }

        public IFilterBuilder<TModel> Lt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Lt(field, value);
        }

        public IFilterBuilder<TModel> Lte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return FilterCreator<TModel>.Lte(field, value);
        }


        public IFilterBuilder<TModel> In<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values) {
            return FilterCreator<TModel>.In(field, values);
        }

        public IFilterBuilder<TModel> Nin<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values) {
            return FilterCreator<TModel>.Nin(field, values);
        }


        public IFilterBuilder<TModel> AnyEq<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyEq(field, value);
        }

        public IFilterBuilder<TModel> AnyNe<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyNe(field, value);
        }

        public IFilterBuilder<TModel> AnyGt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyGt(field, value);
        }

        public IFilterBuilder<TModel> AnyGte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyGte(field, value);
        }

        public IFilterBuilder<TModel> AnyLt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyLt(field, value);
        }

        public IFilterBuilder<TModel> AnyLte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return FilterCreator<TModel>.AnyLte(field, value);
        }


        public IFilterBuilder<TModel> All<TField>(Expression<Func<TModel, IEnumerable<TField>>> field, IEnumerable<TField> values) {
            return FilterCreator<TModel>.All(field, values);
        }

        public IFilterBuilder<TModel> AnyIn<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return FilterCreator<TModel>.AnyIn(field, values);
        }

        public IFilterBuilder<TModel> AnyNin<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return FilterCreator<TModel>.AnyNin(field, values);
        }

        public IFilterBuilder<TModel> ElemMatch<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, Func<IUnaryFilterBuilder<TItem>, IFilterBuilder<TItem>> build) {
            return FilterCreator<TModel>.ElemMatch(field, build);
        }

        public IFilterBuilder<TModel> Exists(Expression<Func<TModel, object>> field, bool exists = true) {
            return FilterCreator<TModel>.Exists(field, exists);
        }

        public IFilterBuilder<TModel> Mod(Expression<Func<TModel, object>> field, long modulus, long remainder) {
            return FilterCreator<TModel>.Mod(field, modulus, remainder);
        }

        public IFilterBuilder<TModel> BitsAllClear(Expression<Func<TModel, object>> field, long bitmask) {
            return FilterCreator<TModel>.BitsAllClear(field, bitmask);
        }

        public IFilterBuilder<TModel> BitsAllSet(Expression<Func<TModel, object>> field, long bitmask) {
            return FilterCreator<TModel>.BitsAllSet(field, bitmask);
        }

        public IFilterBuilder<TModel> BitsAnyClear(Expression<Func<TModel, object>> field, long bitmask) {
            return FilterCreator<TModel>.BitsAnyClear(field, bitmask);
        }

        public IFilterBuilder<TModel> BitsAnySet(Expression<Func<TModel, object>> field, long bitmask) {
            return FilterCreator<TModel>.BitsAnySet(field, bitmask);
        }

        public IFilterBuilder<TModel> Size(Expression<Func<TModel, object>> field, int size) {
            return FilterCreator<TModel>.Size(field, size);
        }

        public IFilterBuilder<TModel> SizeGt(Expression<Func<TModel, object>> field, int size) {
            return FilterCreator<TModel>.SizeGt(field, size);
        }

        public IFilterBuilder<TModel> SizeGte(Expression<Func<TModel, object>> field, int size) {
            return FilterCreator<TModel>.SizeGte(field, size);
        }

        public IFilterBuilder<TModel> SizeLt(Expression<Func<TModel, object>> field, int size) {
            return FilterCreator<TModel>.SizeLt(field, size);
        }

        public IFilterBuilder<TModel> SizeLte(Expression<Func<TModel, object>> field, int size) {
            return FilterCreator<TModel>.SizeLte(field, size);
        }

        public IFilterBuilder<TModel> Where(Expression<Func<TModel, bool>> expression) {
            return FilterCreator<TModel>.Where(expression);
        }
    }
}
