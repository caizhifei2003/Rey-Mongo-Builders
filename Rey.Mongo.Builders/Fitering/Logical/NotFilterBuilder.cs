﻿using MongoDB.Driver;
using System;

namespace Rey.Mongo.Builders.Fitering {
    public class NotFilterBuilder<TModel> : UnaryFilterBuilder<TModel, INotFilterBuilder<TModel>>, INotFilterBuilder<TModel> {
        public NotFilterBuilder(Func<INotFilterBuilder<TModel>, IFilterBuilder<TModel>> build)
            : base(build) {
        }

        protected override FilterDefinition<TModel> Build(FilterDefinition<TModel> filter) {
            return Builders<TModel>.Filter.Not(filter);
        }
    }
}
