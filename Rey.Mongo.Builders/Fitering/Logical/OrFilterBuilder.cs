﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace Rey.Mongo.Builders.Fitering {
    public class OrFilterBuilder<TModel> : PolyFilterBuilder<TModel, IOrFilterBuilder<TModel>>, IOrFilterBuilder<TModel> {
        public OrFilterBuilder(Action<IOrFilterBuilder<TModel>> build)
            : base(build) {
        }

        protected override FilterDefinition<TModel> Build(IEnumerable<FilterDefinition<TModel>> filters) {
            return Builders<TModel>.Filter.Or(filters);
        }
    }
}
