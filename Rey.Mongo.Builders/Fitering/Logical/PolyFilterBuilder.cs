﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders.Fitering {
    public abstract class PolyFilterBuilder<TModel, TBuilder> : FilterBuilder<TModel>, IPolyFilterBuilder<TModel>
        where TBuilder : class, IPolyFilterBuilder<TModel> {
        private List<IFilterBuilder<TModel>> _builders = new List<IFilterBuilder<TModel>>();
        private Action<TBuilder> _build;

        public PolyFilterBuilder(Action<TBuilder> build) {
            this._build = build;
        }

        public override FilterDefinition<TModel> Build() {
            this._build.Invoke(this as TBuilder);

            var filters = this._builders
                .Select(x => x.Build())
                .Where(x => x != FilterDefinition<TModel>.Empty)
                .ToList();

            if (filters.Count == 0)
                return FilterDefinition<TModel>.Empty;

            if (filters.Count == 1)
                return filters.First();

            return this.Build(filters);
        }

        protected abstract FilterDefinition<TModel> Build(IEnumerable<FilterDefinition<TModel>> filters);

        public IPolyFilterBuilder<TModel> Eq<TField>(Expression<Func<TModel, TField>> field, TField value) {
            this._builders.Add(FilterCreator<TModel>.Eq(field, value));
            return this;
        }

        public IPolyFilterBuilder<TModel> Ne<TField>(Expression<Func<TModel, TField>> field, TField value) {
            this._builders.Add(FilterCreator<TModel>.Ne(field, value));
            return this;
        }

        public IPolyFilterBuilder<TModel> Gt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            this._builders.Add(FilterCreator<TModel>.Gt(field, value));
            return this;
        }

        public IPolyFilterBuilder<TModel> Gte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            this._builders.Add(FilterCreator<TModel>.Gte(field, value));
            return this;
        }

        public IPolyFilterBuilder<TModel> Lt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            this._builders.Add(FilterCreator<TModel>.Lt(field, value));
            return this;
        }

        public IPolyFilterBuilder<TModel> Lte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            this._builders.Add(FilterCreator<TModel>.Lte(field, value));
            return this;
        }


        public IPolyFilterBuilder<TModel> In<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values) {
            this._builders.Add(FilterCreator<TModel>.In(field, values));
            return this;
        }

        public IPolyFilterBuilder<TModel> Nin<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values) {
            this._builders.Add(FilterCreator<TModel>.Nin(field, values));
            return this;
        }


        public IPolyFilterBuilder<TModel> AnyEq<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            this._builders.Add(FilterCreator<TModel>.AnyEq(field, value));
            return this;
        }

        public IPolyFilterBuilder<TModel> AnyNe<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            this._builders.Add(FilterCreator<TModel>.AnyNe(field, value));
            return this;
        }

        public IPolyFilterBuilder<TModel> AnyGt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            this._builders.Add(FilterCreator<TModel>.AnyGt(field, value));
            return this;
        }

        public IPolyFilterBuilder<TModel> AnyGte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            this._builders.Add(FilterCreator<TModel>.AnyGte(field, value));
            return this;
        }

        public IPolyFilterBuilder<TModel> AnyLt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            this._builders.Add(FilterCreator<TModel>.AnyLt(field, value));
            return this;
        }

        public IPolyFilterBuilder<TModel> AnyLte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            this._builders.Add(FilterCreator<TModel>.AnyLte(field, value));
            return this;
        }


        public IPolyFilterBuilder<TModel> All<TField>(Expression<Func<TModel, IEnumerable<TField>>> field, IEnumerable<TField> values) {
            this._builders.Add(FilterCreator<TModel>.All(field, values));
            return this;
        }

        public IPolyFilterBuilder<TModel> AnyIn<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            this._builders.Add(FilterCreator<TModel>.AnyIn(field, values));
            return this;
        }

        public IPolyFilterBuilder<TModel> AnyNin<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            this._builders.Add(FilterCreator<TModel>.AnyNin(field, values));
            return this;
        }

        public IPolyFilterBuilder<TModel> ElemMatch<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, Func<IUnaryFilterBuilder<TItem>, IFilterBuilder<TItem>> build) {
            this._builders.Add(FilterCreator<TModel>.ElemMatch(field, build));
            return this;
        }

        public IPolyFilterBuilder<TModel> Exists(Expression<Func<TModel, object>> field, bool exists = true) {
            this._builders.Add(FilterCreator<TModel>.Exists(field, exists));
            return this;
        }

        public IPolyFilterBuilder<TModel> Mod(Expression<Func<TModel, object>> field, long modulus, long remainder) {
            this._builders.Add(FilterCreator<TModel>.Mod(field, modulus, remainder));
            return this;
        }

        public IPolyFilterBuilder<TModel> BitsAllClear(Expression<Func<TModel, object>> field, long bitmask) {
            this._builders.Add(FilterCreator<TModel>.BitsAllClear(field, bitmask));
            return this;
        }

        public IPolyFilterBuilder<TModel> BitsAllSet(Expression<Func<TModel, object>> field, long bitmask) {
            this._builders.Add(FilterCreator<TModel>.BitsAllSet(field, bitmask));
            return this;
        }

        public IPolyFilterBuilder<TModel> BitsAnyClear(Expression<Func<TModel, object>> field, long bitmask) {
            this._builders.Add(FilterCreator<TModel>.BitsAnyClear(field, bitmask));
            return this;
        }

        public IPolyFilterBuilder<TModel> BitsAnySet(Expression<Func<TModel, object>> field, long bitmask) {
            this._builders.Add(FilterCreator<TModel>.BitsAnySet(field, bitmask));
            return this;
        }

        public IPolyFilterBuilder<TModel> Size(Expression<Func<TModel, object>> field, int size) {
            this._builders.Add(FilterCreator<TModel>.Size(field, size));
            return this;
        }

        public IPolyFilterBuilder<TModel> SizeGt(Expression<Func<TModel, object>> field, int size) {
            this._builders.Add(FilterCreator<TModel>.Size(field, size));
            return this;
        }

        public IPolyFilterBuilder<TModel> SizeGte(Expression<Func<TModel, object>> field, int size) {
            this._builders.Add(FilterCreator<TModel>.Size(field, size));
            return this;
        }

        public IPolyFilterBuilder<TModel> SizeLt(Expression<Func<TModel, object>> field, int size) {
            this._builders.Add(FilterCreator<TModel>.Size(field, size));
            return this;
        }

        public IPolyFilterBuilder<TModel> SizeLte(Expression<Func<TModel, object>> field, int size) {
            this._builders.Add(FilterCreator<TModel>.Size(field, size));
            return this;
        }

        public IPolyFilterBuilder<TModel> Where(Expression<Func<TModel, bool>> expression) {
            this._builders.Add(FilterCreator<TModel>.Where(expression));
            return this;
        }
    }
}
