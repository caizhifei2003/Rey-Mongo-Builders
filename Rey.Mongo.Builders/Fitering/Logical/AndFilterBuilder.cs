﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace Rey.Mongo.Builders.Fitering {
    public class AndFilterBuilder<TModel> : PolyFilterBuilder<TModel, IAndFilterBuilder<TModel>>, IAndFilterBuilder<TModel> {
        public AndFilterBuilder(Action<IAndFilterBuilder<TModel>> build)
            : base(build) {
        }

        protected override FilterDefinition<TModel> Build(IEnumerable<FilterDefinition<TModel>> filters) {
            return Builders<TModel>.Filter.And(filters);
        }
    }
}
