﻿using MongoDB.Driver;
using System;

namespace Rey.Mongo.Builders.Fitering {
    public class DefaultUnaryFilterBuilder<TModel> : UnaryFilterBuilder<TModel, INotFilterBuilder<TModel>>, INotFilterBuilder<TModel> {
        public DefaultUnaryFilterBuilder(Func<INotFilterBuilder<TModel>, IFilterBuilder<TModel>> build)
            : base(build) {
        }

        protected override FilterDefinition<TModel> Build(FilterDefinition<TModel> filter) {
            return filter;
        }
    }
}
