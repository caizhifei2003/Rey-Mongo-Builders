﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders.Fitering {
    public interface IFilterBuilder<TModel> {
        FilterDefinition<TModel> Build();

        IFilterBuilder<TModel> Not(Func<INotFilterBuilder<TModel>, IFilterBuilder<TModel>> build);
        IFilterBuilder<TModel> And(Action<IAndFilterBuilder<TModel>> build);
        IFilterBuilder<TModel> Or(Action<IOrFilterBuilder<TModel>> build);
    }

    public interface ILogicialFilterBuilder<TModel> : IFilterBuilder<TModel> { }

    public interface IUnaryFilterBuilder<TModel> : ILogicialFilterBuilder<TModel> {
        IFilterBuilder<TModel> Eq<TField>(Expression<Func<TModel, TField>> field, TField value);
        IFilterBuilder<TModel> Ne<TField>(Expression<Func<TModel, TField>> field, TField value);
        IFilterBuilder<TModel> Gt<TField>(Expression<Func<TModel, TField>> field, TField value);
        IFilterBuilder<TModel> Gte<TField>(Expression<Func<TModel, TField>> field, TField value);
        IFilterBuilder<TModel> Lt<TField>(Expression<Func<TModel, TField>> field, TField value);
        IFilterBuilder<TModel> Lte<TField>(Expression<Func<TModel, TField>> field, TField value);

        IFilterBuilder<TModel> In<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values);
        IFilterBuilder<TModel> Nin<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values);

        IFilterBuilder<TModel> AnyEq<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IFilterBuilder<TModel> AnyNe<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IFilterBuilder<TModel> AnyGt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IFilterBuilder<TModel> AnyGte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IFilterBuilder<TModel> AnyLt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IFilterBuilder<TModel> AnyLte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);

        IFilterBuilder<TModel> All<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> value);
        IFilterBuilder<TModel> AnyIn<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values);
        IFilterBuilder<TModel> AnyNin<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values);

        IFilterBuilder<TModel> ElemMatch<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, Func<IUnaryFilterBuilder<TItem>, IFilterBuilder<TItem>> build);

        IFilterBuilder<TModel> Exists(Expression<Func<TModel, object>> field, bool exists = true);
        IFilterBuilder<TModel> Mod(Expression<Func<TModel, object>> field, long modulus, long remainder);

        IFilterBuilder<TModel> BitsAllClear(Expression<Func<TModel, object>> field, long bitmask);
        IFilterBuilder<TModel> BitsAllSet(Expression<Func<TModel, object>> field, long bitmask);
        IFilterBuilder<TModel> BitsAnyClear(Expression<Func<TModel, object>> field, long bitmask);
        IFilterBuilder<TModel> BitsAnySet(Expression<Func<TModel, object>> field, long bitmask);

        IFilterBuilder<TModel> Size(Expression<Func<TModel, object>> field, int size);
        IFilterBuilder<TModel> SizeGt(Expression<Func<TModel, object>> field, int size);
        IFilterBuilder<TModel> SizeGte(Expression<Func<TModel, object>> field, int size);
        IFilterBuilder<TModel> SizeLt(Expression<Func<TModel, object>> field, int size);
        IFilterBuilder<TModel> SizeLte(Expression<Func<TModel, object>> field, int size);

        //IFilterBuilder<TModel> GeoIntersects<TCoordinates>(Expression<Func<TModel, object>> field, GeoJsonGeometry<TCoordinates> geometry) where TCoordinates : GeoJsonCoordinates;
        //IFilterBuilder<TModel> GeoWithin<TCoordinates>(Expression<Func<TModel, object>> field, GeoJsonGeometry<TCoordinates> geometry) where TCoordinates : GeoJsonCoordinates;
        //IFilterBuilder<TModel> GeoWithinBox(Expression<Func<TModel, object>> field, double lowerLeftX, double lowerLeftY, double upperRightX, double upperRightY);
        //IFilterBuilder<TModel> GeoWithinCenter(Expression<Func<TModel, object>> field, double x, double y, double radius);
        //IFilterBuilder<TModel> GeoWithinCenterSphere(Expression<Func<TModel, object>> field, double x, double y, double radius);
        //IFilterBuilder<TModel> GeoWithinPolygon(Expression<Func<TModel, object>> field, double[,] points);

        //IFilterBuilder<TModel> Near(Expression<Func<TModel, object>> field, double x, double y, double? maxDistance = null, double? minDistance = null);
        //IFilterBuilder<TModel> Near<TCoordinates>(Expression<Func<TModel, object>> field, GeoJsonPoint<TCoordinates> point, double? maxDistance = null, double? minDistance = null) where TCoordinates : GeoJsonCoordinates;
        //IFilterBuilder<TModel> NearSphere(Expression<Func<TModel, object>> field, double x, double y, double? maxDistance = null, double? minDistance = null);
        //IFilterBuilder<TModel> NearSphere<TCoordinates>(Expression<Func<TModel, object>> field, GeoJsonPoint<TCoordinates> point, double? maxDistance = null, double? minDistance = null) where TCoordinates : GeoJsonCoordinates;

        //IFilterBuilder<TModel> OfType<TDerived>(Expression<Func<TDerived, bool>> derivedDocumentFilter) where TDerived : TModel;
        //IFilterBuilder<TModel> OfType<TDerived>() where TDerived : TModel;
        //IFilterBuilder<TModel> OfType<TField, TDerived>(Expression<Func<TModel, TField>> field, Expression<Func<TDerived, bool>> derivedFieldFilter) where TDerived : TField;
        //IFilterBuilder<TModel> OfType<TField, TDerived>(Expression<Func<TModel, TField>> field) where TDerived : TField;

        //IFilterBuilder<TModel> Type(Expression<Func<TModel, object>> field, BsonType type);
        //IFilterBuilder<TModel> Type(Expression<Func<TModel, object>> field, string type);

        //IFilterBuilder<TModel> Regex(Expression<Func<TModel, object>> field, BsonRegularExpression regex);



        //IFilterBuilder<TModel> Text(string search, TextSearchOptions options = null);
        //IFilterBuilder<TModel> Text(string search, string language);

        IFilterBuilder<TModel> Where(Expression<Func<TModel, bool>> expression);
    }

    public interface IPolyFilterBuilder<TModel> : ILogicialFilterBuilder<TModel> {
        IPolyFilterBuilder<TModel> Eq<TField>(Expression<Func<TModel, TField>> field, TField value);
        IPolyFilterBuilder<TModel> Ne<TField>(Expression<Func<TModel, TField>> field, TField value);
        IPolyFilterBuilder<TModel> Gt<TField>(Expression<Func<TModel, TField>> field, TField value);
        IPolyFilterBuilder<TModel> Gte<TField>(Expression<Func<TModel, TField>> field, TField value);
        IPolyFilterBuilder<TModel> Lt<TField>(Expression<Func<TModel, TField>> field, TField value);
        IPolyFilterBuilder<TModel> Lte<TField>(Expression<Func<TModel, TField>> field, TField value);

        IPolyFilterBuilder<TModel> In<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> value);
        IPolyFilterBuilder<TModel> Nin<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values);

        IPolyFilterBuilder<TModel> AnyEq<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IPolyFilterBuilder<TModel> AnyNe<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IPolyFilterBuilder<TModel> AnyGt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IPolyFilterBuilder<TModel> AnyGte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IPolyFilterBuilder<TModel> AnyLt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IPolyFilterBuilder<TModel> AnyLte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);

        IPolyFilterBuilder<TModel> All<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> value);
        IPolyFilterBuilder<TModel> AnyIn<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values);
        IPolyFilterBuilder<TModel> AnyNin<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values);

        IPolyFilterBuilder<TModel> ElemMatch<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, Func<IUnaryFilterBuilder<TItem>, IFilterBuilder<TItem>> build);

        IPolyFilterBuilder<TModel> Exists(Expression<Func<TModel, object>> field, bool exists = true);
        IPolyFilterBuilder<TModel> Mod(Expression<Func<TModel, object>> field, long modulus, long remainder);

        IPolyFilterBuilder<TModel> BitsAllClear(Expression<Func<TModel, object>> field, long bitmask);
        IPolyFilterBuilder<TModel> BitsAllSet(Expression<Func<TModel, object>> field, long bitmask);
        IPolyFilterBuilder<TModel> BitsAnyClear(Expression<Func<TModel, object>> field, long bitmask);
        IPolyFilterBuilder<TModel> BitsAnySet(Expression<Func<TModel, object>> field, long bitmask);

        IPolyFilterBuilder<TModel> Size(Expression<Func<TModel, object>> field, int size);
        IPolyFilterBuilder<TModel> SizeGt(Expression<Func<TModel, object>> field, int size);
        IPolyFilterBuilder<TModel> SizeGte(Expression<Func<TModel, object>> field, int size);
        IPolyFilterBuilder<TModel> SizeLt(Expression<Func<TModel, object>> field, int size);
        IPolyFilterBuilder<TModel> SizeLte(Expression<Func<TModel, object>> field, int size);


        IPolyFilterBuilder<TModel> Where(Expression<Func<TModel, bool>> expression);
    }

    public interface INotFilterBuilder<TModel> : IUnaryFilterBuilder<TModel> { }
    public interface IAndFilterBuilder<TModel> : IPolyFilterBuilder<TModel> { }
    public interface IOrFilterBuilder<TModel> : IPolyFilterBuilder<TModel> { }
}
